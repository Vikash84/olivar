# Olivar
Olivar is an open source pipeline for comprehensive design of primer and probe sets for pathogen 
sequences. Our pipeline relies on the state-of-the-art primer generating software, and incorporates 
low frequency variant information to ensure robust sensitivity and specificity for rapidly evolving 
pathogens. Additionally, our software provides visualization and validation capabilities to aid fast 
evaluation of the output probe and primer sets.

### Install
To use Olivar, simply clone the repository and ensure that the requrements below are installed and in the system path.

### Requirements
* Biopython
* Pandas
* Jinja2
* Parsnp (>= 1.5.\*)
* Primer3
* Blast (>= 2.9.0)
* MAFFT
* PyVCF
* pysam
* tqdm

#### Additional requirements for simulation
1.  Samtools
2.  Bcftools
3.  Minimap2
4.  Bowtie2
5.  Lofreq

### Testing
TODO

### Usage
To run Olivar and generate probes and primers for a set of sequences and a reference, 

```
olivar /path/to/reference.fasta all -G /path/to/input_genomes/* --email <your_email>
```

Alternatively, if the set of genomes exceeds the limit for number of command line arguments, 
you can provide a single file containing absolute paths to genomes, one per line.

```
olivar /path/to/reference.fasta all -F /path/to/genome_list --email <your_email>
```

If you have a set of read datasets you'd like to use to simulate more input data: 
```
olivar /path/to/reference.fasta all -G /path/to/input_genomes/* \
    --simulate 
    --reads    /path/to/read_directory
    --csv      /path/to/read_metadata.csv
    --email <your_email>
```
NCBI may throttle your blast queries if you only provide your email. If you have an API, you can provide that instead to have a higher allowance. Alternatively, you can supply a local blast database location via `--blastdb <database_location>`.

If your configuration of BLAST/Entrez ID is stable you can fill out the `blast_config.ini`
file with the respective information and pass it to Olivar using the `--entrez-config`
option (supported by `all` and `validate` commands).

Please see the `--help` documentation for more sub-commands and arguments.

#### Running individual modules
Olivar allows easy modular execution of any of the included submodules to further
speedup potential re-design and refinement process. Additionally the `scheme` 
submodule runs PrimalScheme on the data allowing the design of sequencing primers.

##### Scheme
*Note:* due to current limitations in PrimalScheme we perform a more stringent quality
check on the genomes than the user specified parameters. Specifically, we do not allow
any ambiguous bases in the genomes used for the PrimalScheme run. 

To run Olivar in sequencing primer generating mode use the following command,
```
olivar path/to/reference.fasta scheme -G /path/to/input_genomes/*
```

Please see `olivar scheme --help` for more documentation on the supported parameters for
this mode.

##### Simulate
Performs read mapping and variant calling for a set of read datasets (Illumina, Oxford 
Nanopore) and then creates genomes containing single nucleotide variants identified by
variant calling. 
```
olivar /path/to/reference.fasta simulate -s --reads path/to/read_directory --csv /path/to/read_metadata.csv
```

Since this operation is costly for the subsequent runs you can use the `--use-existing path/to/simulated_genomes_dir` flag to re-use the simulated genomes. This feature can 
also be useful if you want to use a different variant caller or read mapping tool.

##### Filter
Runs quality filtering on the provided genomes. The parameters controlling filter's 
behavior can be modified in `data/default_config.ini` subsection `[Filter]` or by 
providing a path to alternative configuration file using `-c` flag in Olivar.
```
olivar /path/to/reference.fasta filter -G /path/to/input_genomes/*
```

*Note*: in order to include simulated genomes into the filtering process either 
`-s/--simulate` or `--use-existing` flag should be set, otherwise simulated genomes
will be ignored and not included in the resulting multiple sequence alignment, e.g.
```
olivar /path/to/reference.fasta filter -G /path/to/input_genomes/* --use-existing /path/to/simulated_genomes_dir
```

##### Probes
```
olivar /path/to/reference.fasta probes -G /path/to/input_genomes/*
```

##### Validate
```
olivar /path/to/reference.fasta validate --use-existing /path/to/simulated_genomes_dir --blastdb path/to/blastdb --email your_email58@example.com
```

##### Visualize
```
olivar /path/to/reference.fasta visualize
```

### Output
The final output reports are present in `output/final_report`. In `output`, intermediate directories 
for the other modules' output is present as well.

